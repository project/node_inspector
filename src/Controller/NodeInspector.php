<?php

namespace Drupal\node_inspector\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;

/**
 * Returns responses for Node Inspector routes.
 */
class NodeInspector extends ControllerBase {

  /**
   * Returns a render-able array for a Node inspector page.
   */
  public function inspectNode(NodeInterface $node) {

    $build['#attached']['library'][] = 'node_inspector/inspect-node';
    $build['#cache']['max-age'] = 0;

    // Get info for the node.
    $nodeStorage = $this->entityTypeManager()->getStorage('node');

    $build['node'] = $this->getNodeOutput($node);

    // Get node revisions in reverse order.
    $revisionIds = array_reverse($nodeStorage->revisionIds($node));

    // Get info for each revision.
    foreach ($revisionIds as $revisionId) {

      $revision = $this->entityTypeManager()
        ->getStorage('node')
        ->loadRevision($revisionId);

      $build['revision-' . $revisionId] = $this->getRevisionOutput($revision);
    }

    return $build;
  }

  /**
   * Returns a render-able array of the nodes details.
   */
  private function getNodeOutput($node) {

    $nodeId = $node->Id();
    $title = $node->getTitle();
    $bundle = $node->bundle();
    $owner = $node->getOwner();
    $loadedRevisionId = $node->getLoadedRevisionId();
    $isPublished = $node->isPublished() ? $this->t("Yes") : $this->t("No");
    $isDefaultRevision = $node->isDefaultRevision() ? $this->t("Yes") : $this->t("No");
    $isLatestRevision = $node->isLatestRevision() ? $this->t("Yes") : $this->t("No");
    $changedTime = $node->getChangedTime();
    $fieldDefs = $node->getFieldDefinitions();

    $fieldProperties = [];

    foreach ($fieldDefs as $fieldDef) {

      $fieldName = $fieldDef->getName();
      $fieldType = $fieldDef->getType();
      $fieldIsComputed = $fieldDef->isComputed() ? $this->t("Yes") : $this->t("No");
      $fieldIsTranslatable = $fieldDef->isTranslatable() ? $this->t("Yes") : $this->t("No");
      $fieldIsRequired = $fieldDef->isRequired() ? $this->t("Yes") : $this->t("No");

      $fieldSettings = $fieldDef->getSettings();

      $fieldMaxLength = $fieldSettings['max_length'] ?? '';

      if (method_exists($fieldDef, 'isRevisionAble')) {
        $fieldIsRevisionable = $fieldDef->isRevisionable() ? $this->t("Yes") : $this->t("No");
      }
      else {
        $fieldIsRevisionable = 'n/a';
      }

      $fieldProperties[] = [
        'name' => $fieldName,
        'type' => $fieldType,
        'computed' => $fieldIsComputed,
        'translatable' => $fieldIsTranslatable,
        'required' => $fieldIsRequired,
        'revisionable' => $fieldIsRevisionable,
        'maxLength' => $fieldMaxLength,
      ];
    }

    $build = [
      '#type' => 'details',
      '#title' => $this->t('Node: @nodeId, Latest? @latest, Default? @default, Published? @published', [
        '@nodeId' => $nodeId,
        '@default' => $isDefaultRevision,
        '@latest' => $isLatestRevision,
        '@published' => $isPublished,
      ]),
      '#open' => TRUE,
      '#summary_attributes' => [],
      '#attributes' => [
        'class' => ['node-inspector', 'level-0', 'node'],
      ],
    ];

    $build['properties'] = [
      '#theme' => 'node_inspector_node_properties',
      '#properties' => [
        'title' => $title,
        'bundle' => $bundle,
        'nodeId' => $nodeId,
        'ownerAccountName' => $owner ? $owner->getAccountName() : $this->t('Unknown'),
        'revisionId' => $loadedRevisionId,
        'isPublished' => $isPublished,
        'isDefaultRevision' => $isDefaultRevision,
        'isLatestRevision' => $isLatestRevision,
        'changedTime' => $changedTime,
        'fields' => $fieldProperties,
      ],
    ];

    return $build;
  }

  /**
   * Returns a render-able array of a revisions details.
   */
  private function getRevisionOutput($revision) {

    $title = $revision->getTitle();
    $loadedRevisionId = $revision->getLoadedRevisionId();
    $revisionUser = $revision->getRevisionUser();
    $isPublished = $revision->isPublished() ? $this->t("Yes") : $this->t("No");
    $isDefaultRevision = $revision->isDefaultRevision() ? $this->t("Yes") : $this->t("No");
    $isLatestRevision = $revision->isLatestRevision() ? $this->t("Yes") : $this->t("No");
    $changedTime = $revision->getChangedTime();

    $build = [
      '#type' => 'details',
      '#title' => $this->t('Revision: @revision, Latest? @latest, Default? @default, Published? @published', [
        '@revision' => $loadedRevisionId,
        '@default' => $isDefaultRevision,
        '@latest' => $isLatestRevision,
        '@published' => $isPublished,
      ]),
      '#open' => FALSE,
      '#attributes' => [
        'id' => 'revision-' . $loadedRevisionId,
        'class' => ['node-inspector', 'level-0', 'revision'],
      ],
    ];

    $build['properties'] = [
      '#theme' => 'node_inspector_revision_properties',
      '#properties' => [
        'title' => $title,
        'revisionId' => $loadedRevisionId,
        'revisionAccountName' => $revisionUser ? $revisionUser->getAccountName() : $this->t('Unknown'),
        'isPublished' => $isPublished,
        'isDefaultRevision' => $isDefaultRevision,
        'isLatestRevision' => $isLatestRevision,
        'changedTime' => $changedTime,
      ],
    ];

    if ($revision->hasField('layout_builder__layout')) {

      $sections = $revision->layout_builder__layout->getSections();

      foreach ($sections as $sectionKey => $section) {
        $build['section' . $sectionKey] = $this->getSectionOutput($sectionKey, $section);
      }
    }

    return $build;
  }

  /**
   * Returns a render-able array of a layout sections details.
   */
  private function getSectionOutput($sectionKey, $section) {

    $layoutId = $section->getLayoutId();
    $layoutLabel = $section->getLayoutSettings()['label'];

    $build = [
      '#type' => 'details',
      '#title' => $this->t('Section (@id) - @label', [
        '@id' => $layoutId,
        '@label' => $layoutLabel,
      ]),
      '#open' => FALSE,
      '#attributes' => [
        'class' => ['level-1', 'layout-section'],
      ],
    ];

    $build['properties'] = [
      '#theme' => 'node_inspector_section_properties',
      '#properties' => [
        'layoutId' => $layoutId,
        'layoutLabel' => $layoutLabel,
      ],
    ];

    $components = $section->getComponents();

    foreach ($components as $componentKey => $component) {
      $build['section' . $sectionKey]['component' . $componentKey] = $this->getComponentOutput($component);
    }

    return $build;
  }

  /**
   * Returns a render-able array for the layout component/block details.
   */
  private function getComponentOutput($component) {

    $componentUuid = $component->getUuid();

    $config = $component->get('configuration');

    $id = $config['id'];
    $label = $config['label'];
    $blockType = explode(':', $id)[0];

    $build = [
      '#type' => 'details',
      '#title' => $this->t('Component @id', ['@id' => $id]),
      '#open' => FALSE,
      '#attributes' => [
        'class' => ['level-2', 'layout-component'],
      ],
    ];

    if ($blockType == 'inline_block') {

      $blockRevisionId = $config['block_revision_id'] ?? NULL;

      $blockInstance = $this->entityTypeManager()
        ->getStorage('block_content')
        ->loadRevision($blockRevisionId);

      $blockId = $blockInstance->Id();

      $isReusable = $blockInstance->isReusable() ? $this->t("Yes") : $this->t("No");

      $build['properties'] = [
        '#theme' => 'node_inspector_component_properties',
        '#properties' => [
          'blockType' => "Inline",
          'id' => $id,
          'label' => $label,
          'componentUuid' => $componentUuid,
          'isReusable' => $isReusable,
          'blockId' => $blockId,
          'blockRevisionId' => $blockRevisionId,
        ],
      ];

    }
    elseif ($blockType == 'field_block') {

      $build['properties'] = [
        '#theme' => 'node_inspector_component_properties',
        '#properties' => [
          'blockType' => "Field",
          'id' => $id,
          'label' => $label,
          'componentUuid' => $componentUuid,
        ],
      ];

    }
    else {

      $build['properties'] = [
        '#theme' => 'node_inspector_component_properties',
        '#properties' => [
          'blockType' => "Other",
          'id' => $id,
          'label' => $label,
          'componentUuid' => $componentUuid,
        ],
      ];
    }

    return $build;
  }

}
